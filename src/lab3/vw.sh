vw -d vw_train.txt -P 10000 --loss_function=logistic -p logistic_train.txt -f model_logistic
vw -d vw_train.txt -P 10000 --loss_function=hinge -p hinge_train.txt -f model_hinge
vw -d vw_test.txt -P 10000 --loss_function=hinge -p hinge_prediction.txt -t -i model_hinge
vw -d vw_test.txt -P 10000 --loss_function=logistic -p logistic_prediction.txt -t -i model_logistic