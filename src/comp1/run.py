

import numpy as np
import pandas as pd



gold_data_initial = pd.read_csv('../data1/gold.csv')
gold_data_initial[gold_data_initial == -1] = np.nan
gold_data = gold_data_initial.dropna()
gold_data_600 = gold_data[gold_data['time'] == 600].set_index('mid')

lh_data_initial = pd.read_csv('../data1/lh.csv')
lh_data_initial[lh_data_initial == -1] = np.nan
lh_data = lh_data_initial.dropna()
lh_data_600 = lh_data[lh_data['time'] == 600].set_index('mid')

heroes = pd.read_csv('../lab1/heroes.csv').set_index('mid')



from sklearn.preprocessing import StandardScaler


def prepare_filled_data(data_initial):
    data_joined = data_initial \
        .join(gold_data_600, how='inner') \
        .join(lh_data_600, how='inner', rsuffix='_lh', lsuffix='_gold')

    gold0 = data_joined['player_0_gold']
    gold1 = data_joined['player_1_gold']
    gold2 = data_joined['player_2_gold']
    gold3 = data_joined['player_3_gold']
    gold4 = data_joined['player_4_gold']
    gold5 = data_joined['player_5_gold']
    gold6 = data_joined['player_6_gold']
    gold7 = data_joined['player_7_gold']
    gold8 = data_joined['player_8_gold']
    gold9 = data_joined['player_9_gold']
    data_joined['gold_sum_radiant'] = gold0 + gold1 + gold2 + gold3 + gold4
    data_joined['gold_min_radiant'] = data_joined[
        ['player_0_gold', 'player_1_gold', 'player_2_gold', 'player_3_gold', 'player_4_gold']].min(axis=1)
    data_joined['gold_max_radiant'] = data_joined[
        ['player_0_gold', 'player_1_gold', 'player_2_gold', 'player_3_gold', 'player_4_gold']].max(axis=1)
    data_joined['gold_sum_dire'] = gold5 + gold6 + gold7 + gold8 + gold9
    data_joined['gold_min_dire'] = data_joined[
        ['player_5_gold', 'player_6_gold', 'player_7_gold', 'player_8_gold', 'player_9_gold']].min(axis=1)
    data_joined['gold_max_dire'] = data_joined[
        ['player_5_gold', 'player_6_gold', 'player_7_gold', 'player_8_gold', 'player_9_gold']].max(axis=1)
    lh0 = data_joined['player_0_lh']
    lh1 = data_joined['player_1_lh']
    lh2 = data_joined['player_2_lh']
    lh3 = data_joined['player_3_lh']
    lh4 = data_joined['player_4_lh']
    lh5 = data_joined['player_5_lh']
    lh6 = data_joined['player_6_lh']
    lh7 = data_joined['player_7_lh']
    lh8 = data_joined['player_8_lh']
    lh9 = data_joined['player_9_lh']
    data_joined['lh_sum_radiant'] = lh0 + lh1 + lh2 + lh3 + lh4
    data_joined['lh_min_radiant'] = data_joined[
        ['player_0_lh', 'player_1_lh', 'player_2_lh', 'player_3_lh', 'player_4_lh']].min(axis=1)
    data_joined['lh_max_radiant'] = data_joined[
        ['player_0_lh', 'player_1_lh', 'player_2_lh', 'player_3_lh', 'player_4_lh']].max(axis=1)
    data_joined['lh_sum_dire'] = lh5 + lh6 + lh7 + lh8 + lh9
    data_joined['lh_min_dire'] = data_joined[
        ['player_5_lh', 'player_6_lh', 'player_7_lh', 'player_8_lh', 'player_9_lh']].min(
        axis=1)
    data_joined['lh_max_dire'] = data_joined[
        ['player_5_lh', 'player_6_lh', 'player_7_lh', 'player_8_lh', 'player_9_lh']].max(
        axis=1)

    data_cleared = data_joined.drop(axis=1, labels=[
        'player_0_gold', 'player_1_gold', 'player_2_gold', 'player_3_gold', 'player_4_gold',
        'player_5_gold', 'player_6_gold', 'player_7_gold', 'player_8_gold', 'player_9_gold',
        'player_0_lh', 'player_1_lh', 'player_2_lh', 'player_3_lh', 'player_4_lh',
        'player_5_lh', 'player_6_lh', 'player_7_lh', 'player_8_lh', 'player_9_lh',
        'time_gold', 'time_lh', 'patch'
    ])

    scaler = StandardScaler()
    scaler.fit(data_cleared)
    radiant_win = data_cleared['radiant_win']
    data_cleared[data_cleared.columns] = scaler.transform(data_cleared[data_cleared.columns])
    data_cleared['radiant_win'] = radiant_win
    return data_cleared.join(heroes, how='inner')




def prepare_unfilled_data(data_initial):
    return data_initial.drop(axis=1, labels=['patch']).join(heroes, how='inner')



import numpy as np
import pandas as pd

from sklearn.preprocessing import StandardScaler
from sklearn.utils import shuffle

# const_prediction = 0.5132561328660673
const_prediction = 0.53941

train_data_initial = shuffle(pd.read_csv('../data1/train.csv').set_index('mid'), random_state=42)
test_data_initial = pd.read_csv('../data1/test.csv').set_index('mid')

data_all = train_data_initial.append(test_data_initial)
data_filled = prepare_filled_data(data_all)
data_unfilled = prepare_unfilled_data(data_all)

train_filled = data_filled.loc[data_filled.index.intersection(list(train_data_initial.index))]
train_filled.index.name = 'mid'
train_X_filled = train_filled.drop(axis=1, labels=['radiant_win'])
train_y_filled = train_filled['radiant_win']

test_filled = data_filled.loc[data_filled.index.intersection(list(test_data_initial.index))]
test_filled.index.name = 'mid'
test_X_filled = test_filled.drop(axis=1, labels=['radiant_win'])
test_y_filled = test_filled['radiant_win']

train_unfilled = data_unfilled.loc[data_unfilled.index.intersection(list(train_data_initial.index))]
train_unfilled.index.name = 'mid'
train_X_unfilled = train_unfilled.drop(axis=1, labels=['radiant_win'])
train_y_unfilled = train_unfilled['radiant_win']

test_unfilled = data_unfilled.loc[data_unfilled.index.intersection(list(test_data_initial.index))]
test_unfilled.index.name = 'mid'
test_X_unfilled = test_unfilled


from sklearn.linear_model import SGDClassifier
from sklearn.base import BaseEstimator, ClassifierMixin


class MyClassifier(BaseEstimator, ClassifierMixin):
    def __init__(
            self,
            data_filled,
            data_unfilled
    ):
        self.data_filled = data_filled
        self.data_unfilled = data_unfilled

    def fit(self, X, y):
        X_filled = self.data_filled.loc[self.data_filled.index.intersection(list(X.index))]
        y_filled = y.loc[self.data_filled.index.intersection(list(y.index))]

        X_unfilled = self.data_unfilled.loc[self.data_unfilled.index.intersection(list(X.index))]
        y_unfilled = y.loc[self.data_unfilled.index.intersection(list(y.index))]

        self.filled = SGDClassifier(loss='log', penalty='elasticnet')
        self.filled.fit(X_filled, y_filled)

        self.unfilled = SGDClassifier(loss='log', penalty='elasticnet')
        self.unfilled.fit(X_unfilled, y_unfilled)

        return self

    def predict(self, X):
        X_filled = self.data_filled.loc[self.data_filled.index.intersection(list(X.index))]
        X_unfilled = self.data_unfilled.loc[self.data_unfilled.index.intersection(list(X.index))]

        predict_filled = self.filled.predict(X_filled)
        predict_unfilled = self.unfilled.predict(X_unfilled)

        predict = predict_unfilled.join(predict_filled, how='left', rsuffix='1', lsuffix='2')[
            ['radiant_win_1', 'radiant_win_2']]
        predict = predict.apply(
            lambda row: row['radiant_win_1'] if np.isnan(row['radiant_win_2']) else row['radiant_win_2'], axis=1)

        return predict

    def predict_proba(self, X):
        X_filled = self.data_filled.loc[self.data_filled.index.intersection(list(X.index))]
        X_unfilled = self.data_unfilled.loc[self.data_unfilled.index.intersection(list(X.index))]

        predict_filled = self.filled.predict_proba(X_filled)
        predict_unfilled = self.unfilled.predict_proba(X_unfilled)

        predict = predict_unfilled.join(predict_filled, how='left', rsuffix='1', lsuffix='2')[
            ['radiant_win_1', 'radiant_win_2']]
        predict = predict.apply(
            lambda row: row['radiant_win_1'] if np.isnan(row['radiant_win_2']) else row['radiant_win_2'], axis=1)

        return predict




clf = MyClassifier(data_filled, data_unfilled)
clf.fit(train_X_filled, train_y_filled)



train_y_filled




