from sklearn.linear_model import SGDClassifier
import numpy as np

from prepare import comp1 as init

sgd = SGDClassifier(loss='log', penalty='elasticnet', n_jobs=-1)

sgd.fit(init.train_X, init.train_y)
predict_X = init.test_X.copy()
# predict_X = init.test_X.copy()
test_prediction = sgd.predict_proba(predict_X)
predict_X['radiant_win'] = test_prediction[:, 1]

test_answer_cleared = predict_X.join(init.test_data_initial, how='right')
test_answer_cleared = test_answer_cleared[['radiant_win']].fillna(1)
# test_answer_cleared = test_answer_cleared[['radiant_win']].fillna(init.const_prediction)
print(len(test_answer_cleared))
# open('answer_sgd_norm_2.csv', 'w').write(test_answer_cleared.to_csv())

