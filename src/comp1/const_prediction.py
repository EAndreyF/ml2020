from sklearn.dummy import DummyRegressor


from prepare import comp1 as init

dummy = DummyRegressor(strategy="mean")
train_X = init.train_data_initial.drop(axis=1, labels=['radiant_win', 'patch'])
train_y = init.train_data_initial['radiant_win']
dummy.fit(init.train_X, init.train_y)

predict_X = init.test_data_initial.drop(axis=1, labels=['patch'])
test_prediction = dummy.predict(predict_X)
predict_X['radiant_win'] = test_prediction

test_answer_cleared = predict_X
open('answer_const.csv', 'w').write(test_answer_cleared.to_csv())
