from sklearn.model_selection import GridSearchCV
from sklearn.neighbors import KNeighborsClassifier
import pandas as pd

from prepare import comp1 as init

n_neighbors = range(200, 400, 1)
neighbors = KNeighborsClassifier()
clf0 = GridSearchCV(neighbors, {'n_neighbors': n_neighbors}, cv=5, n_jobs=4)
out = clf0.fit(init.train_X, init.train_y)
open('grid.json', 'w').write(pd.DataFrame(out.cv_results_).to_json())

