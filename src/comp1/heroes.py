from sklearn.feature_extraction import DictVectorizer
import pandas as pd

heroes_data = pd.read_csv('../data1/heroes.csv').set_index('mid')

def create(row):
    d = dict()
    d['radiant_' + str(row['player_0'])] = 1
    d['radiant_' + str(row['player_1'])] = 1
    d['radiant_' + str(row['player_2'])] = 1
    d['radiant_' + str(row['player_3'])] = 1
    d['radiant_' + str(row['player_4'])] = 1
    d['dire_' + str(row['player_5'])] = 1
    d['dire_' + str(row['player_6'])] = 1
    d['dire_' + str(row['player_7'])] = 1
    d['dire_' + str(row['player_8'])] = 1
    d['dire_' + str(row['player_9'])] = 1

    return d


heroes = heroes_data[0:5].apply(create, axis=1)
v = DictVectorizer(sparse=False, dtype=int)
transformed = v.fit_transform(heroes)
df = pd.DataFrame(transformed, columns=[v.get_feature_names()])
df.to_csv()
df = df.set_index('mid')
print(df.head())
a=1
