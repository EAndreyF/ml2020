from sklearn.linear_model import SGDClassifier
from sklearn.metrics import roc_auc_score
import numpy as np

from prepare import comp1 as init

sgd = SGDClassifier(loss='log', penalty='elasticnet', n_jobs=-1)

sgd.fit(init.train_X_extended, init.train_y)
# predict_X = init.train_X_extended.copy()
predict_X = init.test_X_extended.copy()
test_prediction = sgd.predict_proba(predict_X)
predict_X['radiant_win'] = test_prediction[:, 1]

sgd_h = SGDClassifier(loss='log', penalty='elasticnet', n_jobs=-1)
sgd_h.fit(init.train_X_heroes, init.train_y_heroes)
# predict_X_h = init.train_X_heroes.copy()
predict_X_h = init.test_X_heroes.copy()
test_prediction_h = sgd_h.predict_proba(predict_X_h)
predict_X_h['radiant_win'] = test_prediction_h[:, 1]

test_answer_cleared = predict_X\
    .join(predict_X_h, how='right', rsuffix='_1', lsuffix='_2')\
    .join(init.test_data_initial, how='right')\
    [['radiant_win_1', 'radiant_win_2']]

test_answer_cleared['radiant_win'] = test_answer_cleared\
    .apply(lambda row: row['radiant_win_1'] if np.isnan(row['radiant_win_2']) else row['radiant_win_2'], axis=1)
test_answer_cleared = test_answer_cleared.drop(axis=1, labels=['radiant_win_1', 'radiant_win_2'])

# print(test_answer_cleared)
# score = roc_auc_score(list(init.train_y_initial), list(test_answer_cleared['radiant_win']))
# print(score)
print(len(test_answer_cleared))
open('answer_sgd_extended_fill_nan2.csv', 'w').write(test_answer_cleared.to_csv())
