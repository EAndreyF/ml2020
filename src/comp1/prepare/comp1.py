import numpy as np
import pandas as pd

from sklearn.preprocessing import StandardScaler
from sklearn.utils import shuffle

# const_prediction = 0.5132561328660673
const_prediction = 0.53941


def prepare_data(data_initial):
    data_joined = data_initial \
        .join(gold_data_600, how='inner') \
        .join(lh_data_600, how='inner', rsuffix='_lh', lsuffix='_gold')

    gold0 = data_joined['player_0_gold']
    gold1 = data_joined['player_1_gold']
    gold2 = data_joined['player_2_gold']
    gold3 = data_joined['player_3_gold']
    gold4 = data_joined['player_4_gold']
    gold5 = data_joined['player_5_gold']
    gold6 = data_joined['player_6_gold']
    gold7 = data_joined['player_7_gold']
    gold8 = data_joined['player_8_gold']
    gold9 = data_joined['player_9_gold']
    data_joined['gold_sum_radiant'] = gold0 + gold1 + gold2 + gold3 + gold4
    data_joined['gold_min_radiant'] = data_joined[
        ['player_0_gold', 'player_1_gold', 'player_2_gold', 'player_3_gold', 'player_4_gold']].min(axis=1)
    data_joined['gold_max_radiant'] = data_joined[
        ['player_0_gold', 'player_1_gold', 'player_2_gold', 'player_3_gold', 'player_4_gold']].max(axis=1)
    data_joined['gold_sum_dire'] = gold5 + gold6 + gold7 + gold8 + gold9
    data_joined['gold_min_dire'] = data_joined[
        ['player_5_gold', 'player_6_gold', 'player_7_gold', 'player_8_gold', 'player_9_gold']].min(axis=1)
    data_joined['gold_max_dire'] = data_joined[
        ['player_5_gold', 'player_6_gold', 'player_7_gold', 'player_8_gold', 'player_9_gold']].max(axis=1)
    lh0 = data_joined['player_0_lh']
    lh1 = data_joined['player_1_lh']
    lh2 = data_joined['player_2_lh']
    lh3 = data_joined['player_3_lh']
    lh4 = data_joined['player_4_lh']
    lh5 = data_joined['player_5_lh']
    lh6 = data_joined['player_6_lh']
    lh7 = data_joined['player_7_lh']
    lh8 = data_joined['player_8_lh']
    lh9 = data_joined['player_9_lh']
    data_joined['lh_sum_radiant'] = lh0 + lh1 + lh2 + lh3 + lh4
    data_joined['lh_min_radiant'] = data_joined[
        ['player_0_lh', 'player_1_lh', 'player_2_lh', 'player_3_lh', 'player_4_lh']].min(axis=1)
    data_joined['lh_max_radiant'] = data_joined[
        ['player_0_lh', 'player_1_lh', 'player_2_lh', 'player_3_lh', 'player_4_lh']].max(axis=1)
    data_joined['lh_sum_dire'] = lh5 + lh6 + lh7 + lh8 + lh9
    data_joined['lh_min_dire'] = data_joined[['player_5_lh', 'player_6_lh', 'player_7_lh', 'player_8_lh', 'player_9_lh']].min(
        axis=1)
    data_joined['lh_max_dire'] = data_joined[['player_5_lh', 'player_6_lh', 'player_7_lh', 'player_8_lh', 'player_9_lh']].max(
        axis=1)

    data_cleared = data_joined.drop(axis=1, labels=[
        'player_0_gold', 'player_1_gold', 'player_2_gold', 'player_3_gold', 'player_4_gold',
        'player_5_gold', 'player_6_gold', 'player_7_gold', 'player_8_gold', 'player_9_gold',
        'player_0_lh', 'player_1_lh', 'player_2_lh', 'player_3_lh', 'player_4_lh',
        'player_5_lh', 'player_6_lh', 'player_7_lh', 'player_8_lh', 'player_9_lh',
        'time_gold', 'time_lh', 'patch'
    ])

    return data_cleared


train_data_initial = pd.read_csv('../data1/train.csv').set_index('mid')
test_data_initial = pd.read_csv('../data1/test.csv').set_index('mid')
gold_data_initial = pd.read_csv('../data1/gold.csv')
lh_data_initial = pd.read_csv('../data1/lh.csv')

gold_data_initial[gold_data_initial == -1] = np.nan
gold_data = gold_data_initial.dropna()
lh_data_initial[lh_data_initial == -1] = np.nan
lh_data = lh_data_initial.dropna()

gold_data_600 = gold_data[gold_data['time'] == 600].set_index('mid')
lh_data_600 = lh_data[lh_data['time'] == 600].set_index('mid')

train = prepare_data(train_data_initial)
test_X = prepare_data(test_data_initial)

train = shuffle(train, random_state=42)

train_X = train.drop(axis=1, labels=['radiant_win'])
train_y = train['radiant_win']
train_y_initial = train_data_initial['radiant_win']

scaler = StandardScaler()
train_X_normalized = train_X.copy()
test_X_normalized = test_X.copy()
scaler.fit(train_X_normalized)
train_X_normalized[train_X_normalized.columns] = scaler.transform(train_X_normalized[train_X_normalized.columns])
test_X_normalized[test_X_normalized.columns] = scaler.transform(test_X_normalized[test_X_normalized.columns])

heroes = pd.read_csv('../lab1/heroes.csv').set_index('mid')
train_X_extended = train_X_normalized.join(heroes, how='inner')
test_X_extended = test_X_normalized.join(heroes, how='inner')

train_X_heroes = train_data_initial.drop(axis=1, labels=['patch', 'radiant_win']).join(heroes, how='inner')
test_X_heroes = test_data_initial.drop(axis=1, labels=['patch']).join(heroes, how='inner')
train_y_heroes = train_data_initial['radiant_win']

train_X_extended.to_csv('t.csv')