from sklearn.neighbors import KNeighborsClassifier
import numpy as np

from prepare import comp1 as init

neighbors = KNeighborsClassifier(n_neighbors=422)
# neighbors = KNeighborsClassifier(n_neighbors=4)

neighbors.fit(init.train_X, init.train_y)
print(len(init.train_y))
predict_X = init.test_X.copy()
# test_prediction = neighbors.predict(predict_X)
test_prediction = neighbors.predict_proba(predict_X)
predict_X['radiant_win'] = test_prediction[:, 1]

test_answer_cleared = predict_X.join(init.test_data_initial, how='right')
test_answer_cleared = test_answer_cleared[['radiant_win']].fillna(init.const_prediction)
print(len(test_answer_cleared))
# print(test_answer_cleared)
open('answer_knn_norm_422.csv', 'w').write(test_answer_cleared.to_csv())

