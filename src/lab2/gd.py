import numpy as np
import pandas as pd
import pylab as plt
import math
train_X = pd.read_csv('train_X.csv').set_index('id')
train_y = pd.read_csv('train_y.csv').set_index('id')
validate_X = pd.read_csv('validate_X.csv').set_index('id')
validate_y = pd.read_csv('validate_y.csv').set_index('id')
X = pd.read_csv('train_X_full.csv').set_index('id')
y = pd.read_csv('train_y_full.csv').set_index('id')
test_X = pd.read_csv('test_X.csv').set_index('id')
from sklearn.base import BaseEstimator, ClassifierMixin
from sklearn.metrics import log_loss
import random

from scipy.special import expit


class MyClassifierAdgard(BaseEstimator, ClassifierMixin):
    loss_functions = {
        "log": (log_loss,),
    }

    def __init__(
            self,
            loss='log',
            penalty='l2',
            max_iter=10000,
            tol=1e-4,
            learning_rate='const',
            eta0=1,
            w=None,
            verbose=0,
            C=0,
            n_iter_no_change=5,
            eps=1e-8,
    ):
        self.loss = loss
        self.loss_func = loss
        self.penalty = penalty
        self.max_iter = max_iter
        self.tol = tol
        self.learning_rate = learning_rate
        self.eta0 = eta0
        self.verbose = verbose
        self.C = C
        self.n_iter_no_change = n_iter_no_change
        self.eps = eps

        self.coeff_sum = None
        self.w = w
        self.iter = 0
        self.eta = 0
        self.iter_total = 0
        self.coef_ = None
        self.iter_error = []
        self.error = None

    def get_loss_func(self):
        return MyClassifier.loss_functions[self.loss_func][0]

    def fit(self, _X, _y):
        X = np.array(_X)
        y = np.array(_y)

        if self.w is None:
            self.coef_ = np.zeros((X.shape[1], 1))
        else:
            self.coef_ = self.w.copy()

        # Добавляем ещё один признак который будет заменять регуляризатор b
        X = np.append(np.ones((X.shape[0], 1)), X, axis=1) * y

        # Сохраняем начальную инициализацию весов
        self.coef_ = np.append(np.zeros((1, 1)), self.coef_, axis=0)
        coef_sum = self.coef_.copy()

        coef = self.coef_ * self.coef_
        coef_norm = np.linalg.norm(coef)
        no_change = self.n_iter_no_change

        for i in range(self.max_iter):
            nu = self.get_learning_rate(i)
            error = self.error_calc(X, y, coef)

            if error == np.inf:
                break

            if self.verbose > 1:
                self.iter_error.append([i, error])

            coef = self.next_iter(coef, X, y, nu, self.eps, coef_sum)
            coef_new_norm = np.linalg.norm(coef)

            if abs(coef_norm - coef_new_norm) < self.tol:
                if no_change == 1:
                    break
                else:
                    no_change -= 1
            else:
                no_change = self.n_iter_no_change

            self.coef_ = coef
            coef_sum += coef * coef
            coef_norm = coef_new_norm

        self.iter_total = i
        self.error = self.error_calc(X, y, coef)

        if self.verbose:
            print('iter total:', self.iter_total)

        return self

    def get_learning_rate(self, n_iter):
        if self.learning_rate == 'const':
            return self.eta0

    def prob(self, X, w):
        prob = expit(X @ w)
        return np.hstack([1 - prob, prob])

    def error_calc(self, X, y, w):
        N = X.shape[0]
        return np.sum(np.log(1 + np.exp(-X @ w))) / N

    def loss_calc(self, X, y, w):
        prob = self.prob(X, w)
        return self.get_loss_func()(y, prob)

    def next_iter(self, w, X, y, nu, eps, coef_sum):
        N = X.shape[0]
        next_i = random.randrange(0, N)
        row = X[next_i:next_i + 1]
        gradient_matrix = row.T @ (1 - expit(row @ w))
        g = np.sqrt(coef_sum + eps)
        w_new = w + nu * gradient_matrix / g

        return w_new

    def predict_proba(self, _X):
        X = _X.copy()
        X = np.append(np.ones((X.shape[0], 1)), X, axis=1)
        prob = self.prob(X, self.coef_)
        return prob


res = []
clfs = []
for eta0 in [4e-5]:  # np.arange(1e-4, 2e-3, 1e-4):
    clf = MyClassifierAdgard(max_iter=10000, verbose=2, learning_rate='const', eta0=eta0, tol=1e-6, eps=1e-4,
                             n_iter_no_change=5)
    clf.fit(train_X, train_y)
    clfs.append(clf)
    res.append([eta0, clf.iter_total, clf.error])

df_iter_adgard = pd.DataFrame(res, columns=['eta0', 'iter', 'error'])
print(clfs[0].iter_error)