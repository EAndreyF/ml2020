import numpy as np
import pandas as pd

X = pd.read_csv('train_X.csv').set_index('id')
y = pd.read_csv('train_y.csv').set_index('id')
y[y['label'] == -1] = 0
X = np.array(X)
y = np.array(y)

def sigmoid(z):
    return 1/ (1 + np.exp(-z))


def costFunction(theta, X, y):
    """
    Функция потерь логистической регрессии
    """
    m = len(y)

    predictions = sigmoid(np.dot(X, theta))
    error = (-y * np.log(predictions)) - ((1 - y) * np.log(1 - predictions))

    cost = 1 / m * sum(error)

    grad = 1 / m * np.dot(X.transpose(), (predictions - y))

    return cost[0], grad


def gradientDescent(X, y, theta, alpha, num_iters):
    """
    Take in numpy array X, y and theta and update theta by taking num_iters gradient steps
    with learning rate of alpha

    return theta and the list of the cost of theta during each iteration
    """

    m = len(y)
    J_history = []

    for i in range(num_iters):
        cost, grad = costFunction(theta, X, y)
        theta = theta - (alpha * grad)
        J_history.append(cost)

    return theta, J_history

m , n = X.shape[0], X.shape[1]
X= np.append(np.ones((m,1)),X,axis=1)
initial_theta = np.zeros((n+1,1))
theta , J_history = gradientDescent(X,y,initial_theta,1,400)
print("Theta optimized by gradient descent:",theta)
print("The cost of the optimized theta:",J_history[-1])